-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 19 2017 г., 16:59
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_gallows`
--

-- --------------------------------------------------------

--
-- Структура таблицы `histories`
--

CREATE TABLE `histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `word_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `histories`
--

INSERT INTO `histories` (`id`, `user_id`, `word_id`, `created_at`, `updated_at`) VALUES
(21, 202249874, 7, '2017-04-19 13:09:54', '2017-04-19 13:09:54'),
(22, 202249874, 4, '2017-04-19 13:09:55', '2017-04-19 13:09:55'),
(23, 202249874, 9, '2017-04-19 13:10:03', '2017-04-19 13:10:03'),
(24, 202249874, 3, '2017-04-19 13:11:13', '2017-04-19 13:11:13'),
(25, 174366505, 5, '2017-04-19 13:15:42', '2017-04-19 13:15:42'),
(26, 174366505, 4, '2017-04-19 13:18:16', '2017-04-19 13:18:16'),
(27, 174366505, 8, '2017-04-19 13:20:18', '2017-04-19 13:20:18'),
(28, 347475362, 4, '2017-04-19 13:31:38', '2017-04-19 13:31:38'),
(29, 347475362, 9, '2017-04-19 13:34:22', '2017-04-19 13:34:22'),
(30, 332071135, 4, '2017-04-19 13:46:08', '2017-04-19 13:46:08');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2017_04_18_204405_create_words_table', 1),
(6, '2017_04_18_204440_create_histories_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attempts` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `word_id` int(11) NOT NULL,
  `in_game` tinyint(1) NOT NULL,
  `command` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guessed_letters` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `attempts`, `word_id`, `in_game`, `command`, `guessed_letters`, `created_at`, `updated_at`) VALUES
(174366505, 1, 8, 0, 'check', 'маух', '2017-04-19 05:35:20', '2017-04-19 13:33:31'),
(332071135, 2, 4, 0, 'check', 'мза', '2017-04-19 13:03:44', '2017-04-19 13:46:47'),
(202249874, 2, 3, 0, 'check', 'мзда', '2017-04-19 13:09:53', '2017-04-19 13:12:03'),
(347475362, 3, 9, 0, 'check', 'тамя', '2017-04-19 13:31:25', '2017-04-19 13:35:03');

-- --------------------------------------------------------

--
-- Структура таблицы `words`
--

CREATE TABLE `words` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `words`
--

INSERT INTO `words` (`id`, `value`) VALUES
(1, 'мгла'),
(2, 'мера'),
(3, 'мзда'),
(4, 'муза'),
(5, 'мина'),
(6, 'мода'),
(7, 'мука'),
(8, 'муха'),
(9, 'мята');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD KEY `users_id_index` (`id`);

--
-- Индексы таблицы `words`
--
ALTER TABLE `words`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `words`
--
ALTER TABLE `words`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
