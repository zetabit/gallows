<?php

namespace App\Console\Commands;

use App\Word;
use Illuminate\Console\Command;

class load extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lines = file_get_contents(storage_path('app').DIRECTORY_SEPARATOR.'litc-win.txt');
        $arr = explode("\n", $lines);
        for($i=0, $added = 0; $i<sizeof($arr); $i++) {
            $line = $arr[$i];
            $ar = explode(" ", trim($line) );
            if(sizeof($ar)>2 || sizeof($ar)<2) continue;
            $line = $ar[1];
            if(mb_strlen($line)>4) {
                Word::create([
                    'value' => $line
                ]);
                $added++;
            }
            if($added>10000) break;
        }
    }
}
