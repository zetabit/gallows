<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = ['word_id', 'user_id'];

    /**
     * У истории есть слово
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function word(){
        return $this->belongsTo(Word::class);
    }

    /**
     * И у истории есть пользватель который это слово угадывал
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function user(){
        return $this->belongsTo(User::class);
    }
}
