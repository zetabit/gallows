<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'command', 'guessed_letters', 'attempts', 'word_id', 'in_game', 'pushed'
    ];
    public $incrementing = false;

    function history() {
        return $this->hasMany(History::class);
    }

    function word() {
        return $this->belongsTo(Word::class);
    }
}
