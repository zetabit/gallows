<?php
namespace Telegram\Bot\Commands;

use App\History;
use App\User;
use App\Word;
use DateTime;
use \Telegram\Bot\Actions;
use \Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartCommand extends Command
{
    protected $name = "start";
    protected $description = "Начните с команды start";
    protected $message, $user, $user_id, $text;

    public function handle($arguments)
    {
        $update = $this->getUpdate();
        $this->message = $message = $update->getMessage();
        $chat = $message!=null ? $message->getChat() : null;
        $from = $message!=null ? $message->getFrom() : null;
        $message_id = $message!=null ? $message->getMessageId() : -1;
        $this->user_id = $user_id = $from!=null ? $from->getId() : -1;
        $this->user = \App\User::find($this->user_id);
        $text = $this->text = $message!=null ? $message->getText() : null;
        $user = $this->user;
        $text = $this->text;

        $update = $this->getUpdate();
        $message = $update->getMessage();
        $from = $message!=null ? $message->getFrom() : null;
        $chat = $message!=null ? $message->getChat() : null;
        $user_id = $from!=null ? $from->getId() : -1;
        if($user_id!=$chat->getId()){   //если сообщение из группы
        }

        if($user) {
            $rez = $this->getWord($user);
            if($rez==0){
                $this->replyWithMessage(['text' => 'У меня закончились слова :( Используй /clear']);
            }elseif($rez>0){
                $user->pushed = "";
                $user->save();
                $reply_markup = CheckCommand::genKeyboard($user->guessed_letters, $user->pushed);
                $text = CheckCommand::genResult($user);
                $this->replyWithMessage([
                    'text'=>$text,
                    'parse_mode' => 'Html',
                    'reply_markup' => $reply_markup
                ]);
            }
        }
    }

    public function getWord(User $user){
        global $bNotChangeCommand;
        $words = Word::all()->pluck('id')->toArray();   //total words
        $items = $user->history()->get()->pluck('word_id')->toArray();  //history words
        $diff = array_diff($words, $items);
        $diff = array_values($diff);
        if( sizeof($diff)==0 || sizeof($words) == sizeof($items) ) {
            return 0;
        }
        $numb = sizeof($diff)==1 ? 0 : rand(0, sizeof($diff)-1);
        $bNotChangeCommand = true;
        $user->command = "check";
        $user->attempts = 0;
        $user->word_id = $diff[$numb];
        $user->guessed_letters = "";
        $user->save();
        $history = History::where([
            ['user_id', '=', $user->id],
            ['word_id', '=', $diff[$numb]]
        ])->get();
        if(sizeof($history)==0) History::create([
            'user_id' => $user->id,
            'word_id' => $diff[$numb],
        ]);
        return $diff[$numb];
    }
}

