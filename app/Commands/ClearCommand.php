<?php
namespace Telegram\Bot\Commands;

use App\History;
use App\User;
use App\Word;
use DateTime;
use \Telegram\Bot\Actions;
use Telegram\Bot\Api;
use \Telegram\Bot\Commands\Command;

class ClearCommand extends Command
{
    protected $name = "clear";
    protected $description = "слова заново";
    protected $message, $user, $user_id, $text;

    public function handle($arguments)
    {
        $update = $this->getUpdate();
        $this->message = $message = $update->getMessage();
        $chat = $message!=null ? $message->getChat() : null;
        $from = $message!=null ? $message->getFrom() : null;
        $message_id = $message!=null ? $message->getMessageId() : -1;
        $this->user_id = $user_id = $from!=null ? $from->getId() : -1;
        $this->user = \App\User::find($this->user_id);
        $text = $this->text = $message!=null ? $message->getText() : null;
        $user = $this->user;
        $text = $this->text;

        $update = $this->getUpdate();
        $message = $update->getMessage();
        $from = $message!=null ? $message->getFrom() : null;
        $chat = $message!=null ? $message->getChat() : null;
        $user_id = $from!=null ? $from->getId() : -1;
        if($user_id!=$chat->getId()){   //если сообщение из группы
        }

        if($user) {
            $history = History::where([
                ['user_id','=', $user->id]
            ])->get();
            foreach ($history as $item) $item->delete();
            $user->pushed = "";
            $user->save();
            $this->replyWithMessage(['text'=>'История успешно очищена, используйте /start для игры']);
        }
    }
}

