<?php
namespace Telegram\Bot\Commands;

use App\User;
use App\Word;
use DateTime;
use \Telegram\Bot\Actions;
use Telegram\Bot\Api;
use \Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;

class CheckCommand extends Command
{
    protected $name = "check";
    protected $description = "режим проверки слова";
    protected $message, $user, $user_id, $text;

    public function handle($arguments)
    {
        $update = $this->getUpdate();
        $this->message = $message = $update->getMessage();
        $chat = $message!=null ? $message->getChat() : null;
        $from = $message!=null ? $message->getFrom() : null;
        $message_id = $message!=null ? $message->getMessageId() : -1;
        $this->user_id = $user_id = $from!=null ? $from->getId() : -1;
        $this->user = \App\User::find($this->user_id);
        $text = $this->text = $message!=null ? $message->getText() : null;
        $user = $this->user;
        $text = $this->text;

        $update = $this->getUpdate();
        $message = $update->getMessage();
        $from = $message!=null ? $message->getFrom() : null;
        $chat = $message!=null ? $message->getChat() : null;
        $user_id = $from!=null ? $from->getId() : -1;
        if($user_id!=$chat->getId()){   //если сообщение из группы
        }

        if($user) {
            $rez = CheckCommand::addAttempt($user, mb_strtolower($text));
            $reply_markup = CheckCommand::genKeyboard($user->guessed_letters, $user->pushed);
            if(mb_strlen($text) != 1) {
                $this->replyWithMessage([
                    'text' => 'Можно отгадывать только по 1 букве. Попробуйте написать букву.',
                    'reply_markup' => $reply_markup
                ]);
                return;
            }
            if($rez===1) {
                $this->replyWithMessage([
                    'text' => 'Угадали букву!',
                    'reply_markup' => $reply_markup
                ]);
            }elseif ($rez===-2) {
                $this->replyWithMessage([
                    'text' => 'Закончились попытки, слово : '.$user->word->value.', /start для нового слова',
                    'reply_markup' => Keyboard::hide()
                ]);
                return;
            }elseif ($rez===-1) {
                $this->replyWithMessage([
                    'text' => 'Такой буквы нет!',
                    'reply_markup' => $reply_markup
                ]);
            }elseif ($rez===0) {
                $this->replyWithMessage([
                    'text' => 'Такая буква уже отгадывалась',
                    'reply_markup' => $reply_markup
                ]);
            }elseif ($rez==="") {
                $this->replyWithMessage([
                    'text' => 'Нет загаданного слова, попробуйте /start',
                    'reply_markup' => Keyboard::hide()
                ]);
            }elseif ($rez==2) {
                $this->replyWithMessage([
                    'text' => 'Поздравляем. Угадали слово! новое слово /start',
                    'reply_markup' => Keyboard::hide()
                ]);
            }
            $t = CheckCommand::genResult($user);
            if(strlen($t) > 0)
                $this->replyWithMessage([
                    'text' => $t,
                    'parse_mode' => 'Html',
                    'reply_markup' => $reply_markup
                ]);
            //проверяем есть ли достаточное кол-во попыток
            //проверяем есть ли данная буква в слове
        }
    }

    /**
     * @param User $user
     * @return string
     */
    static public function genResult(User $user){
        $attempts = $user->attempts;
        $word = $user->word;
        if(!$word) return "";
        $text = "Слово: ".CheckCommand::genWord($word->value, $user->guessed_letters)."\r\n";
        $smiles = "😵😨😔😌😊😜😄";
        $text .= mb_substr($smiles, 0, mb_strlen($smiles) - $attempts)."\r\n";
        return $text;
    }

    static public function genKeyboard($guessed = "", $pushed = "") {
        $string = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        $word_letters = CheckCommand::mb_str_split($string);
        $guessed_letters = CheckCommand::mb_str_split($guessed);
        $pushed_letters = CheckCommand::mb_str_split($pushed);

        $keyboard = [];
        $keyboard[0] = [];
        $iCount = 0; $iLine = 0;
        foreach ($word_letters as $letter){
            if(in_array($letter, $guessed_letters) || in_array($letter, $pushed_letters)) continue;
            if($iLine >= 7) {
                $iCount++;
                $iLine = 0;
                $keyboard[$iCount] = [];
            }
            $keyboard[$iCount][] = $letter;
            $iLine++;
        }
        return Keyboard::make([
            'keyboard' => $keyboard
        ]);
    }

    static public function addPushedLetter(User $user, $letter) {
        $word_letters = CheckCommand::mb_str_split($user->pushed);
        if(!in_array($letter, $word_letters)) {
            $word_letters[] = $letter;
            $user->pushed = implode("", $word_letters);
            $user->save();
        }
    }

    static public function addAttempt(User $user, $letter) {
        $attempts = $user->attempts;
        $guessed  = $user->guessed_letters;
        $word = $user->word;
        CheckCommand::addPushedLetter($user, $letter);  //сохраняем нажатую
        if(!$word) return "";

        if(CheckCommand::checkLetters($guessed, $word->value)) {
            return 2;
        }

        if($attempts > config('app.max_attempts')) {
            return -2;  //закончились попытки
        };

        $word_letters = CheckCommand::mb_str_split($word->value);
        $guessed_letters = CheckCommand::mb_str_split($guessed);

        if(in_array($letter,$guessed_letters)){
            return 0;   //уже отгадывалась буква
        }
        if(in_array($letter, $word_letters)){
            $guessed .= $letter;
            $user->guessed_letters = $guessed;
            $user->save();
            if(CheckCommand::checkLetters($guessed, $word->value)) {
                return 2;
            }
            return 1;
        }

        $attempts++;
        $user->attempts = $attempts;
        $user->save();
        return -1;  //не угадали
    }

    static function checkLetters($guessed, $word) {
        $word_letters = CheckCommand::mb_str_split($word);
        $guessed_letters = CheckCommand::mb_str_split($guessed);
        $result = "";
        for($i=0; $i<sizeof($word_letters); $i++) {
            $letter = $word_letters[$i];
            if(in_array($letter, $guessed_letters)) $result .= $letter;
        }
        return strcmp($result, $word)===0;
    }

    static function mb_str_split($string) {
        return preg_split('#(?<!^)(?!$)#u', $string);
    }

    static public function genWord($word, $guessed) {
        $word_letters = CheckCommand::mb_str_split($word);
        $guessed_letters = CheckCommand::mb_str_split($guessed);
        $result = "";
        for($i=0; $i<sizeof($word_letters); $i++) {
            $letter = $word_letters[$i];
            if($i>0) $result .= " ";
            if(in_array($letter, $guessed_letters)) {
                $result .= $letter;
            } else $result .= "_";
        }
        return $result;
    }
}

