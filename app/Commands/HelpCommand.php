<?php
namespace Telegram\Bot\Commands;

use App\User;
use App\Word;
use DateTime;
use \Telegram\Bot\Actions;
use Telegram\Bot\Api;
use \Telegram\Bot\Commands\Command;

class HelpCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'help';
    /**
     * @var string Command Description
     */
    protected $description = 'Команда "помощь", список всех команд.';

    public function handle($arguments)
    {
        global $bNotChangeCommand;
        $bNotChangeCommand = true;
        $commands = $this->telegram->getCommands();

        $text = 'Цель игры - угадать загаданное слово.

Правила: 
Бот загадывает слово, а вам нужно его угадать. 
Делая предположения, вы отправляете боту букву, а он, в свою очередь, либо сообщает, что такой буквы нет, либо ставит угаданную букву на свое место. 
У вас есть право ошибиться 7 раз, после чего игра заканчивается.
Для того, чтобы выиграть, вам необходимо угадать все слово целиком.';
        $text = $text . "\n" .' Мои команды:'."\n";
        foreach ($commands as $name => $handler) {
            $text .= sprintf('/%s - %s'.PHP_EOL, $name, $handler->getDescription());
        }

        $this->replyWithMessage([
            'text' => $text,
        ]);
    }
}


