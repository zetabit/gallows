<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\TelegramRequest;

class TelegramController extends Controller
{
    protected $telegram;

    public function __construct( )
    {
        $telegram = new Api(config('telegram.bot_token'));
        //$telegram->addCommands($telegram);
        $this->telegram = $telegram;
        // Standalone
    }
    public function setWebhook(){
        $telegram = $this->telegram;
        $response = $telegram->setWebhook(['url' => config('app.url').'/'.config('telegram.bot_token').'/webhook']);
        return $response;
    }

    public function removeWebhook(){
        $telegram = $this->telegram;
        $response = $telegram->removeWebhook();
        return json_encode($response);
    }

    public function hook(){
        global $bNotChangeCommand;
        $bNotChangeCommand = false;
        $user_id = -1;
        info('hook run...');
        $update = null;
        try{
            $telegram = $this->telegram;
            $update = Telegram::commandsHandler(true);
            info(var_export($update, true));
            $update = new Update($update);
            $message = $update->getMessage();
            $from = $message!=null ? $message->getFrom() : null;
            $chat = $message!=null ? $message->getChat() : null;
            $user_id = $from!=null ? $from->getId() : -1;
            $user = \App\User::find($user_id);
            if(!$user) \App\User::create([  //пользователь новый, сохраняем в БД с начальной командой
                'id' => $user_id,
                'command' => 'start',
                'word_id' => 0,
                'in_game' => false,
                'guessed_letters' => "",
                'pushed'    => ""
            ]);
            $user = \App\User::find($user_id);
            $commandBus = Telegram::getCommandBus();
            $text = "";

            if($message !== null && $message->has('text'))  //obtain text
                $text = $message->getText();

            if($chat!=null && $user_id==$chat->getId()) {   //только когда чат не групповой (1на1)
                if (strlen($text) == 0 || $text[0] != '/') {    //если не команда
                    //то вызовем команду у пользователя
                    if ($user != null)
                        $commandBus->execute($user->command, [], $update);
                } else {
                    $match = $commandBus->parseCommand($text);
                    if (!empty($match) && $user!=null && !$bNotChangeCommand) {
                        $command = $match[1];
                        $user->command = $command;
                        $user->save();
                    }
                }
            }elseif($chat!=null && $chat->getId()!=$user_id && $chat->getTitle()!=null){  //сообщение в группе
                $telegram->sendMessage([
                    'chat_id' => $chat->getId(),
                    'text' => 'заготовка для группы, user['.$chat->getId().'] command['.$message->getText()."]\r\n".
                        var_export($update, true)
                ]);
                //заготовка для группы
                //$commandBus->execute('group', [''], $update);
            }
        }
        catch (TelegramSDKException $e){
            if(isset($message)){
                $text = 'user['.$message->getChat()->getId().'] command['.$message->getText()."]\r\n-----------\r\n".$e->getMessage()."\r\n".
                    var_export($update, true)."\r\n trace: \r\n";
                $arr = explode("%|%", wordwrap($text, 4096, "%|%", true));
                for($i=0; $i<2 && $i<sizeof($arr); $i++) {
                    $this->telegram->sendMessage([
                        'chat_id'=>174366505,
                        'text'=>$arr[$i]
                    ]);
                }
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                        'chat_id'=>174366505,
                        'text'=>$arr[$i]
                    ]);
                }
            }
            elseif($user_id>0)
                $this->telegram->sendMessage([
                    'chat_id'=>$user_id,
                    'text' => $e->getMessage()
                ]);
            else {
                $this->telegram->sendMessage([
                    'chat_id'=>174366505,
                    'text' => $e->getMessage()
                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                        'chat_id'=>174366505,
                        'text'=>$arr[$i]
                    ]);
                }
            }
            return "Ok";
        }
        catch (Exception $e){
            if(isset($message)){
                $this->telegram->sendMessage([
                    'chat_id'=>174366505,
                    'text' => 'user['.$message->getChat()->getId().'] command['.$message->getText()."]\r\n------------\r\n".$e->getMessage()."\r\n".
                        var_export($update, true)."\r\n trace: \r\n"
                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                        'chat_id'=>174366505,
                        'text'=>$arr[$i]
                    ]);
                }
            } else {
                $this->telegram->sendMessage([
                    'chat_id'=>174366505,
                    'text' => $e->getMessage()
                ]);
                $trace = $e->getTraceAsString();
                $arr = explode("%|%", wordwrap($trace, 4096, "%|%", true));
                for($i=0; $i<1; $i++) {
                    $this->telegram->sendMessage([
                        'chat_id'=>174366505,
                        'text'=>$arr[$i]
                    ]);
                }
            }
            return "Ok";
        }

        return 'Ok';
    }

    public function getMe(){
        $telegram = $this->telegram;
        return $telegram->getMe();
    }

    public function getCommands(){
        $telegram = $this->telegram;
        return $telegram->getCommands();
    }
}
